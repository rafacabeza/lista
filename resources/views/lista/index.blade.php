@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Mis listas
                </div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    @forelse ($listas as $lista)
                        <tr>
                            <td>
                                {{ $lista->nombre }}
                            </td>
                            <td>

                                <form method="post" action="/listas/{{ $lista->id }}">
                                <a class="btn btn-primary" href="/listas/{{ $lista->id }}">Ver</a>
                                    @csrf
                                    <input type="hidden" name="_method" value="delete">
                                    <input class="btn btn-danger" type="submit" name="" value="borrar">

                                </form>
                            </td>
                        </tr>
                    @empty
                        No hay ninguna lista creada
                    @endforelse
                    </table>
                </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="/listas/create">Nueva</a>
            </div>
            </div>

        </div>
    </div>
</div>
@endsection
