@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Lista: {{ $lista->nombre }}
        </div>

        <div class="card-body">
          <p><strong>Elementos de la lista</strong></p>
          <table class="table">
            @forelse ($lista->elementos as $elemento)
            <tr >
              <td>
                {{ $elemento->texto }}
              </td>
              <td>
                @if ( $elemento->hecho )
                <a href="/elementos/{{ $elemento->id }}/deshacer">
                  <span class="fa fa-check" style="color: green">&nbsp;</span>
                </a>
                @else
                <a href="/elementos/{{ $elemento->id }}/hacer">
                  <span class="fa fa-remove" style="color: red">&nbsp;</span>
                </a>
                @endif
                <a href="/elementos/{{ $elemento->id }}/borrar">
                  <span class="fa fa-trash" style="color: red">&nbsp;</span>
                </a>
              </td>
            </tr>
            @empty
            Lista vacía
            @endforelse
          </table>
        </div>
        <div class="card-footer">
          <form class="form-inline" method="post" action="/listas/{{ $lista->id }}/elementos">
            @csrf
            <div class="form-group">
              <label for="texto">Texto:</label>
              <input type="text" class="form-control" id="texto" placeholder="Añadir texto" name="texto">
            </div>
            <button type="submit" class="btn btn-default">añadir</button>
          </form>

        </div>
      </div>

    </div>
  </div>
</div>
@endsection
