@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Mis listas
        </div>

        <div class="card-body">
            <form method="post" action="/listas">
              @csrf
              <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>
                <div class="col-md-6">
                  <input class="form-control" type="text" name="nombre">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                  <input class="btn btn-primary form-control " type="submit" value="Crear">
                </div>
              </div>
            </form>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
